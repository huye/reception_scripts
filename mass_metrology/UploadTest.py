#!/usr/bin/env python3
'''
example:
    ./UploadTest.py -i flex_mass_20UPGPQ2110456.json
'''

from argparse import ArgumentParser
import itkdb
import json

stages = {
            'PCB': 'PCB_RECEPTION_MODULE_SITE',
            'BARE_MODULE': 'BAREMODULERECEPTION'
        }

if __name__ == "__main__":
    parser = ArgumentParser(
        description="upload testruns to PDB"
    )
    parser.add_argument("-i", dest="input_json", type=str, required=True, help="input json")
    args = parser.parse_args()

    client = itkdb.Client()
    client.user.authenticate()

    input_json = args.input_json
    
    with open(input_json) as f:
        test_data = json.load(f)
        print('input: \n', test_data)

        sn = test_data['component']
        component = client.get('getComponent',json = {"component":sn})

        if component['componentType']['code'] in stages.keys():
            if component['currentStage']['code'] != stages[component['componentType']['code']]:
                print('set stage to %s'%stages[component['componentType']['code']])
                client.post("setComponentStage", json={"component": sn, "stage": stages[component['componentType']['code']]})
 
        test = client.post("uploadTestRunResults", json=test_data)
        print('upload \n', test)
