#!/usr/bin/python3

'''
example:
    python flex_metrology_toJson.py -sn 20UPGPQ2110513 -f raw_data/2023-8-10_flex513.CSV -u AQ
'''


from argparse import ArgumentParser
from sys import exit
from datetime import datetime
import pandas as pd
import numpy as np
import json

points = {
        #"DIAMETER_DOWEL_HOLE_A": [],
        #"WIDTH_DOWEL_SLOT_B": [],
        "X_DIMENSION": ["xb", "xt"],
        "Y_DIMENSION": ["yl", "yr"],
        #"X-Y_DIMENSION_WITHIN_ENVELOP": False,
        "AVERAGE_THICKNESS_FECHIP_PICKUP_AREAS": ["p11","p12","p21","p22","p31","p32","p41","p42"],
        "STD_DEVIATION_THICKNESS_FECHIP_PICKUP_AREAS": ["p11","p12","p21","p22","p31","p32","p41","p42"],
        #"AVERAGE_THICKNESS_FECHIP_PICKUP_AREAS": ["Punkt 30","Punkt 31","Punkt 32","Punkt 33","Punkt 34","Punkt 35","Punkt 36","Punkt 37"],
        #"STD_DEVIATION_THICKNESS_FECHIP_PICKUP_AREAS": ["Punkt 30","Punkt 31","Punkt 32","Punkt 33","Punkt 34","Punkt 35","Punkt 36","Punkt 37"],
        "HV_CAPACITOR_THICKNESS": ["sr","sl"],
        #"HV_CAPACITOR_THICKNESS_WITHIN_ENVELOP": False,
        "AVERAGE_THICKNESS_POWER_CONNECTOR": ["c1","c2","c3","c4"]
        }

units = {
        #"DIAMETER_DOWEL_HOLE_A": "mm",
        #"WIDTH_DOWEL_SLOT_B": "mm",
        "X_DIMENSION": "mm",
        "Y_DIMENSION": "mm",
        #"X-Y_DIMENSION_WITHIN_ENVELOP": False,
        "AVERAGE_THICKNESS_FECHIP_PICKUP_AREAS": "mm",
        "STD_DEVIATION_THICKNESS_FECHIP_PICKUP_AREAS": "mm",
        "HV_CAPACITOR_THICKNESS": "mm",
        #"HV_CAPACITOR_THICKNESS_WITHIN_ENVELOP": False,
        "AVERAGE_THICKNESS_POWER_CONNECTOR": "mm"
        }

hv_envelop = [1.701, 2.111]
x_envelop = [39.5, 39.7]
y_envelop = [40.5, 40.7]


ref = {
        "chuck": "chuck"
        }
 

if __name__ == "__main__":
    parser = ArgumentParser(
        description=" prepare bare module mass json for uploading with WebApp."
    )
    parser.add_argument("-sn", dest="sn", type=str, required=True, help="bare module serial number")
    parser.add_argument("-f", dest="raw_data", type=str, required=True, help="raw data CSV file")
    parser.add_argument("-run", dest="run_no", type=str, default="1", help="run number")
    parser.add_argument("-t", dest="run_date", type=str, help="test date")
    parser.add_argument("-u", dest="user", type=str, default="", help="User")
    args = parser.parse_args()

    sn = args.sn
    raw_data = args.raw_data
    run_no = args.run_no
    run_date = args.run_date
    user = args.user

    if len(sn) != 14 or not sn.startswith("20UP"):
        print("Wrong format serial number is given!")
        exit(0)

    if run_date is None:
        run_date = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")


    df = pd.DataFrame(columns=['point','x','y','z'])

    # string to float
    def to_float(s):
        if s.replace(",","").replace("-","").replace(" ","").isdigit():
            v = float(s.replace(',','.'))
        else:
            v = None
        return v

    # fitted plane only has z value, move to column z in dataframe
    def correct_plane_z(meas):
        if meas[1] is not None and meas[2] is None and meas[3] is None:
            meas[3] = meas[1]
            meas[1] = None
        return meas
    
    # convert hex bytes to a string value for each line
    meas = [""]*4
    i_meas = 0

    with open(raw_data, "rb") as f:
        for l in f:
            # slice the bytes
            r = [l[i:i+1] for i in range(len(l))]
            # decode to string 
            s = ''.join([i.decode('utf-8').replace('\x00','') for i in r]).split('","')

            # title line
            if('Element' in s[0]):
                continue

            # empty line
            if(len(s)< 3):
                df.loc[len(df)] = correct_plane_z(meas)
                continue

            # format data as 'meas_name x [y z]'
            if s[0] !='"':
                if ''.join([i for i in meas if i is not None and isinstance(i, str)]) != '':
                    df.loc[len(df)] = correct_plane_z(meas)
                meas[0] = s[0].replace('"','').lstrip()
                meas[1] = to_float(s[2])
                meas[2] = None
                meas[3] = None
                i_meas = 1
            elif i_meas == 1:
                meas[2] = to_float(s[2])
                i_meas = 2
            elif i_meas == 2:
                meas[3] = to_float(s[2])
                i_meas = 0

    df = df.set_index("point")

    # set z accroding to ref chuck z
    ref_z = df.loc[ref["chuck"]]["z"]
    print("ref chuck z %f"%ref_z)
    df["z"] = df["z"] + ref_z

    #print(df.to_string())

    # prepare json
    meas_json = {
      "component": sn,
      "testType": "METROLOGY",
      "institution": "GOETTINGEN",
      "runNumber": run_no,
      "date": run_date,
      "passed": "true",
      "problems": "false",
      "properties": {
        "OPERATOR": user,
        "INSTRUMENT": "",
        "ANALYSIS_VERSION": ""
      },
      "results": {
        #"X_DIMENSION": 234.64,
        #"Y_DIMENSION": 182.95,
        #"X-Y_DIMENSION_WITHIN_ENVELOP": false,
        #"AVERAGE_THICKNESS_FECHIP_PICKUP_AREAS": 743.97,
        #"STD_DEVIATION_THICKNESS_FECHIP_PICKUP_AREAS": 354.54,
        #"HV_CAPACITOR_THICKNESS": 65.05,
        #"HV_CAPACITOR_THICKNESS_WITHIN_ENVELOP": false,
        #"AVERAGE_THICKNESS_POWER_CONNECTOR": 667.35,
        #"DIAMETER_DOWEL_HOLE_A": 933.68,
        #"WIDTH_DOWEL_SLOT_B": 140.52
      }
    }

    scale = 1
    print("Measurement\tpoints\tvalues")
    for r,p in points.items():
        if units[r] == "um":
            scale = 1000
        elif units[r] == "mm":
            scale = 1

        if r.startswith('X_'):
            print(r, p, df.loc[p,'x'].values)
            meas_json['results'][r] = df.loc[p,'x'].mean() * scale
        elif r.startswith('Y_'):
            print(r, p, df.loc[p,'y'].values)
            meas_json['results'][r] = df.loc[p,'y'].mean() * scale
        elif r.startswith("STD_"):
            print(r, p, df.loc[p,'z'].values)
            meas_json['results'][r] = df.loc[p,'z'].std() * scale
        else:
            print(r, p, df.loc[p,'z'].values)
            meas_json['results'][r] = df.loc[p,'z'].mean() * scale

    print("X envelop", x_envelop)
    print("Y envelop", y_envelop)
    print("HV envelop", hv_envelop)

    if hv_envelop[0]<meas_json['results']["HV_CAPACITOR_THICKNESS"]< hv_envelop[1]:
        meas_json['results']["HV_CAPACITOR_THICKNESS_WITHIN_ENVELOP"] = True
    else:
        meas_json['results']["HV_CAPACITOR_THICKNESS_WITHIN_ENVELOP"] = False

    if x_envelop[0]<meas_json['results']["X_DIMENSION"]<x_envelop[1] and  y_envelop[0]<meas_json['results']["Y_DIMENSION"]<y_envelop[1]:
        meas_json['results']["X-Y_DIMENSION_WITHIN_ENVELOP"] = True
    else:
        meas_json['results']["X-Y_DIMENSION_WITHIN_ENVELOP"] = False
    
    print(meas_json)

    with open('flex_metrology_%s.json'%sn, 'w') as outfile:
        json.dump(meas_json, outfile)
