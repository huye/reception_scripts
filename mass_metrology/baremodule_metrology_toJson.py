#!/usr/bin/python3

'''
example:
    python baremodule_metrology_toJson.py -sn 20UPGB42200006 -f raw_data/2023-8-10_BM8.CSV 
'''


from argparse import ArgumentParser
from sys import exit
from datetime import datetime
import pandas as pd
import numpy as np
import json

points = {
        "SENSOR_X": ["se xb", "se xt"],
        "SENSOR_Y": ["se yr", "se yl"],
        #"SENSOR_THICKNESS": ["1","2","3","4","5","6","7","8","9"],
        #"SENSOR_THICKNESS_STD_DEVIATION": ["1","2","3","4","5","6","7","8","9"],
        "FECHIPS_X": ["fe xb", "fe xt"],
        "FECHIPS_Y": ["fe yr", "fe yl"],
        "FECHIP_THICKNESS": ["fe1","fe2","fe3","fe4"],
        "FECHIP_THICKNESS_STD_DEVIATION": ["fe1","fe2","fe3","fe4"],
        "BARE_MODULE_THICKNESS": ["1","2","3","4","5","6","7","8","9"],
        "BARE_MODULE_THICKNESS_STD_DEVIATION": ["1","2","3","4","5","6","7","8","9"]
        }

units = {
        "SENSOR_X": "mm",
        "SENSOR_Y": "mm",
        #"SENSOR_THICKNESS": "um",
        #"SENSOR_THICKNESS_STD_DEVIATION": "um",
        "FECHIPS_X": "mm",
        "FECHIPS_Y": "mm",
        "FECHIP_THICKNESS": "um",
        "FECHIP_THICKNESS_STD_DEVIATION": "um",
        "BARE_MODULE_THICKNESS": "um",
        "BARE_MODULE_THICKNESS_STD_DEVIATION": "um"
        }

ref = {
        "chuck": "chuck"
        }
 

if __name__ == "__main__":
    parser = ArgumentParser(
        description=" prepare bare module mass json for uploading with WebApp."
    )
    parser.add_argument("-sn", dest="sn", type=str, required=True, help="bare module serial number")
    parser.add_argument("-f", dest="raw_data", type=str, required=True, help="raw data CSV file")
    parser.add_argument("-run", dest="run_no", type=str, default="1", help="run number")
    parser.add_argument("-t", dest="run_date", type=str, help="test date")
    args = parser.parse_args()

    sn = args.sn
    raw_data = args.raw_data
    run_no = args.run_no
    run_date = args.run_date

    if len(sn) != 14 or not sn.startswith("20UP"):
        print("Wrong format serial number is given!")
        exit(0)

    if run_date is None:
        run_date = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")


    df = pd.DataFrame(columns=['point','x','y','z'])

    # string to float
    def to_float(s):
        if s.replace(",","").replace("-","").replace(" ","").isdigit():
            v = float(s.replace(',','.'))
        else:
            v = None
        return v

    # fitted plane only has z value, move to column z in dataframe
    def correct_plane_z(meas):
        if meas[1] is not None and meas[2] is None and meas[3] is None:
            meas[3] = meas[1]
            meas[1] = None
        return meas
    
    # convert hex bytes to a string value for each line
    meas = [""]*4
    i_meas = 0

    with open(raw_data, "rb") as f:
        for l in f:
            # slice the bytes
            r = [l[i:i+1] for i in range(len(l))]
            # decode to string 
            s = ''.join([i.decode('utf-8').replace('\x00','') for i in r]).split('","')

            # title line
            if('Element' in s[0]):
                continue

            # empty line
            if(len(s)< 3):
                df.loc[len(df)] = correct_plane_z(meas)
                continue

            # format data as 'meas_name x [y z]'
            if s[0] !='"':
                if ''.join([i for i in meas if i is not None and isinstance(i, str)]) != '':
                    df.loc[len(df)] = correct_plane_z(meas)
                meas[0] = s[0].replace('"','').lstrip()
                meas[1] = to_float(s[2])
                meas[2] = None
                meas[3] = None
                i_meas = 1
            elif i_meas == 1:
                meas[2] = to_float(s[2])
                i_meas = 2
            elif i_meas == 2:
                meas[3] = to_float(s[2])
                i_meas = 0

    df = df.set_index("point")

    # set z accroding to ref chuck z
    ref_z = df.loc[ref["chuck"]]["z"]
    print("ref chuck z %f"%ref_z)
    df["z"] = df["z"] + ref_z

    #print(df.to_string())

    # prepare json
    meas_json = {
      "component": sn,
      "testType": "QUAD_BARE_MODULE_METROLOGY",
      "institution": "GOETTINGEN",
      "runNumber": run_no,
      "date": run_date,
      "passed": "true",
      "problems": "false",
      "properties": {
        "ANALYSIS_VERSION": ""
      },
      "results": {
        #"SENSOR_X": 414.56,
        #"SENSOR_Y": 373.26,
        #"SENSOR_THICKNESS": 608.08,
        #"SENSOR_THICKNESS_STD_DEVIATION": 756.83,
        #"FECHIPS_X": 164.69,
        #"FECHIPS_Y": 35.91,
        #"FECHIP_THICKNESS": 511.75,
        #"FECHIP_THICKNESS_STD_DEVIATION": 656.58,
        #"BARE_MODULE_THICKNESS": 894.79,
        #"BARE_MODULE_THICKNESS_STD_DEVIATION": 343.71
      }
    }

    scale = 1
    print("Measurement\tpoints\tvalues")
    for r,p in points.items():
        if units[r] == "um":
            scale = 1000
        elif units[r] == "mm":
            scale = 1

        if r.endswith('_X'):
            print(r, p, df.loc[p,'x'].values)
            meas_json['results'][r] = df.loc[p,'x'].mean() * scale
        elif r.endswith('_Y'):
            print(r, p, df.loc[p,'y'].values)
            meas_json['results'][r] = df.loc[p,'y'].mean() * scale
        elif r.endswith("STD_DEVIATION"):
            print(r, p, df.loc[p,'z'].values)
            meas_json['results'][r] = df.loc[p,'z'].std() * scale
        else:
            print(r, p, df.loc[p,'z'].values)
            meas_json['results'][r] = df.loc[p,'z'].mean() * scale
    
    print(meas_json)

    with open('bm_metrology_%s.json'%sn, 'w') as outfile:
        json.dump(meas_json, outfile)
