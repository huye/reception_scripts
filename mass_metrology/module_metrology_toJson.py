#!/usr/bin/python3

'''
example:
    python module_metrology_toJson.py -sn 20UPGM22110094 -f raw_data/2023-8-22_BM6+fl094.CSV 
'''


from argparse import ArgumentParser
from sys import exit
from datetime import datetime
import pandas as pd
import numpy as np
import json

points = {
          "PCB_BAREMODULE_POSITION_TOP_RIGHT": ["flfi1", "bmfi1"],
          "PCB_BAREMODULE_POSITION_BOTTOM_LEFT": ["flfi3", "bmfi3"],
          #"ANGLE_PCB_BM": [],
          "AVERAGE_THICKNESS": ["p11","p12","p21","p22","p31","p32","p41","p42"],
          "STD_DEVIATION_THICKNESS": ["p11","p12","p21","p22","p31","p32","p41","p42"],
          "THICKNESS_VARIATION_PICKUP_AREA": ["p11","p12","p21","p22","p31","p32","p41","p42"],
          "THICKNESS_INCLUDING_POWER_CONNECTOR": ["c1","c2","c3","c4"],
          "HV_CAPACITOR_THICKNESS": ["sl","sr"]
        }

units = {
          "PCB_BAREMODULE_POSITION_TOP_RIGHT": "um",
          "PCB_BAREMODULE_POSITION_BOTTOM_LEFT": "um",
          #"ANGLE_PCB_BM": "deg",
          "AVERAGE_THICKNESS": "um",
          "STD_DEVIATION_THICKNESS": "um",
          "THICKNESS_VARIATION_PICKUP_AREA": "um",
          "THICKNESS_INCLUDING_POWER_CONNECTOR": "um",
          "HV_CAPACITOR_THICKNESS": "um"
        }

ref = {
        "chuck": "chuck"
        }
 

if __name__ == "__main__":
    parser = ArgumentParser(
        description=" prepare bare module mass json for uploading with WebApp."
    )
    parser.add_argument("-sn", dest="sn", type=str, required=True, help="bare module serial number")
    parser.add_argument("-f", dest="raw_data", type=str, required=True, help="raw data CSV file")
    parser.add_argument("-run", dest="run_no", type=str, default="1", help="run number")
    parser.add_argument("-t", dest="run_date", type=str, help="test date")
    args = parser.parse_args()

    sn = args.sn
    raw_data = args.raw_data
    run_no = args.run_no
    run_date = args.run_date

    if len(sn) != 14 or not sn.startswith("20UP"):
        print("Wrong format serial number is given!")
        exit(0)

    if run_date is None:
        run_date = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")


    df = pd.DataFrame(columns=['point','x','y','z'])

    # string to float
    def to_float(s):
        if s.replace(",","").replace("-","").replace(" ","").isdigit():
            v = float(s.replace(',','.'))
        else:
            v = None
        return v

    # fitted plane only has z value, move to column z in dataframe
    def correct_plane_z(meas):
        if meas[1] is not None and meas[2] is None and meas[3] is None:
            meas[3] = meas[1]
            meas[1] = None
        return meas
    
    # convert hex bytes to a string value for each line
    meas = [""]*4
    i_meas = 0

    with open(raw_data, "rb") as f:
        for l in f:
            # slice the bytes
            r = [l[i:i+1] for i in range(len(l))]
            # decode to string 
            s = ''.join([i.decode('utf-8').replace('\x00','') for i in r]).split('","')

            # title line
            if('Element' in s[0]):
                continue

            # empty line
            if(len(s)< 3):
                df.loc[len(df)] = correct_plane_z(meas)
                continue

            # format data as 'meas_name x [y z]'
            if s[0] !='"':
                if ''.join([i for i in meas if i is not None and isinstance(i, str)]) != '':
                    df.loc[len(df)] = correct_plane_z(meas)
                meas[0] = s[0].replace('"','').lstrip()
                meas[1] = to_float(s[2])
                meas[2] = None
                meas[3] = None
                i_meas = 1
            elif i_meas == 1:
                meas[2] = to_float(s[2])
                i_meas = 2
            elif i_meas == 2:
                meas[3] = to_float(s[2])
                i_meas = 0

    df = df.set_index("point")

    # set z accroding to ref chuck z
    ref_z = df.loc[ref["chuck"]]["z"]
    print("ref chuck z %f"%ref_z)
    df["z"] = df["z"] + ref_z

    #print(df.to_string())

    # prepare json
    meas_json = {
      "component": sn,
      "testType": "QUAD_MODULE_METROLOGY",
      "institution": "GOETTINGEN",
      "runNumber": run_no,
      "date": run_date,
      "passed": "true",
      "problems": "false",
      "properties": {
        "ANALYSIS_VERSION": ""
      },
      "results": {
          #"PCB_BAREMODULE_POSITION_TOP_RIGHT": [395.71,395.71],
          #"PCB_BAREMODULE_POSITION_BOTTOM_LEFT": [610.46,610.46],
          #"ANGLE_PCB_BM": 122.49,
          #"AVERAGE_THICKNESS": [19.45,19.45],
          #"STD_DEVIATION_THICKNESS": [289.47,289.47],
          #"THICKNESS_VARIATION_PICKUP_AREA": 123.47,
          #"THICKNESS_INCLUDING_POWER_CONNECTOR": 544.68,
          #"HV_CAPACITOR_THICKNESS": 896.75
      }
    }

    scale = 1
    print("Measurement\tpoints\tvalues")
    for r,p in points.items():
        if units[r] == "um":
            scale = 1000
        elif units[r] == "mm":
            scale = 1

        if r.startswith('PCB_BAREMODULE_POSITION_'):
            print(r, p, df.loc[p,'x'].values, df.loc[p,'y'].values)
            meas_json['results'][r] = [ 
                    np.abs(df.loc[p,'x'].values[0]-df.loc[p,'x'].values[1]) * scale,
                    np.abs(df.loc[p,'y'].values[0]-df.loc[p,'y'].values[1]) * scale
                    ]
        elif r in ["AVERAGE_THICKNESS","STD_DEVIATION_THICKNESS","THICKNESS_VARIATION_PICKUP_AREA"]:
            # if 1 point measured per GA.
            ga_z_avg = df.loc[p,'z'].values
            # if 2 points measured per GA.
            if len(df.loc[p,'z'].values) == 8:
                ga_z_avg = [np.mean(i) * scale for i in np.split(df.loc[p,'z'].values,4)]
            if r == "AVERAGE_THICKNESS":
                print(r, p, ga_z_avg)
                meas_json['results'][r] = ga_z_avg
            elif r == "STD_DEVIATION_THICKNESS":
                print(r, p, np.std(ga_z_avg))
                meas_json['results'][r] = [np.std(ga_z_avg)]
            elif r == "THICKNESS_VARIATION_PICKUP_AREA":
                print(r, p, df.loc[p,'z'].values)
                meas_json['results'][r] = np.max(ga_z_avg) - np.min(ga_z_avg)
        else:
            print(r, p, df.loc[p,'z'].values)
            meas_json['results'][r] = df.loc[p,'z'].mean() * scale
    
    print(meas_json)

    with open('module_metrology_%s.json'%sn, 'w') as outfile:
        json.dump(meas_json, outfile)
