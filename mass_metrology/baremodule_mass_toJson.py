#!/usr/bin/python3

'''
example:
    python baremodule_mass_toJson.py -sn 20UPGB42200008 -m 1209 -r 2
'''


from argparse import ArgumentParser
from sys import exit
from datetime import datetime
import json


if __name__ == "__main__":
    parser = ArgumentParser(
        description=" prepare bare module mass json for uploading with WebApp."
    )
    parser.add_argument("-sn", dest="sn", type=str, required=True, help="bare module serial number")
    parser.add_argument("-m", dest="mass", type=float, required=True, help="mass value of bare module in [mg]")
    parser.add_argument("-run", dest="run_no", type=str, default="1", help="run number")
    parser.add_argument("-t", dest="run_date", type=str, help="test date")
    args = parser.parse_args()

    sn = args.sn
    mass = args.mass
    run_no = args.run_no
    run_date = args.run_date

    if len(sn) != 14 or not sn.startswith("20UP"):
        print("Wrong format serial number is given!")
        exit(0)

    if run_date is None:
        run_date = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")

    json_tile = {
            "component": sn,
            "testType": "MASS_MEASUREMENT",
            "institution": "GOETTINGEN",
            "runNumber": run_no,
            "date": run_date,
            "passed": "true",
            "problems": "false",
            "properties":{
                "SCALE_ACCURACY": 0.1,
                "ANALYSIS_VERSION": ""
                },
            "results":{
                "MASS": mass
                }
            }

    print(json_tile)
    with open("baremodule_mass_%s.json"%sn, "w") as outfile:
        json.dump(json_tile, outfile)



