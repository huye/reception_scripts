#!/usr/bin/python3

'''
example:
    python 1_alt_convertRawToTxt.py -sn 20UPGS33300230 -f rawdata/Q9_IV_1.txt -v 51.7
'''



from argparse import ArgumentParser
from sys import exit
from datetime import datetime
import pandas as pd
import numpy as np



if __name__ == "__main__":
    parser = ArgumentParser(
        description="covent probe station IV data file to the formate used by sensor GUI."
    )
    parser.add_argument("-sn", dest="tile_sn", type=str, required=True, help="sensor tile serial number")
    parser.add_argument("-f", dest="raw_data", type=str, required=True, help="raw data file of probe station IV")
    parser.add_argument("-t", dest="run_date", type=str, help="test date")
    parser.add_argument("-v", dest="v_depl", type=float, default=60, help="depletion voltage")
    args = parser.parse_args()

    tile_sn = args.tile_sn
    raw_data = args.raw_data
    run_date = args.run_date
    v_depl = args.v_depl

    if len(tile_sn) != 14 or not tile_sn.startswith("20UP"):
        print("Wrong format serial number is given!")
        exit(0)

    if run_date is None:
        run_date = datetime.now().strftime("%Y-%m-%d_%H:%M")

    df = pd.read_csv(raw_data, skiprows=6, sep='\t', names=['t/s', 'U/V', 'Iavg/uA', 'Istd/uA', 'T/C', 'RH/%', 'Tchiller/C'])
    df = df.drop(columns='Tchiller/C')
    #df = df.set_index("t/s")
    df = df.mul({'t/s':1, 'U/V':-1, 'Iavg/uA':-1, 'Istd/uA':1, 'T/C':1, 'RH/%':1})
    print(df.head())

    lines = [
            "%s\tIV"%tile_sn,
            "GOETTINGEN\t%s"%run_date,
            "prefix uA",
            "depletion_voltage\t%f"%v_depl,
            "%f\t%f"%(np.mean(df['T/C'].values.tolist()),np.mean(df['RH/%'].values.tolist())),
            ""
            ]
    with open('sensor_iv_%s.txt'%tile_sn, 'w') as outfile:
        outfile.write('\n'.join(lines))

        #outfile.write(df.to_string(index=False))
        data_lines = ['\t'.join(df.columns)]

        for i in df.index:
            data_lines.append('\t'.join([str(v) for v in df.iloc[i].values.flatten().tolist()]))

        outfile.write('\n'.join(data_lines))

        




    
