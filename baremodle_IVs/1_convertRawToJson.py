#!/usr/bin/python3

'''
example:
    python 1_convertRawToJson.py -sn 20UPGS33300230 -f rawdata/Q9_IV_1.txt -v 51.7 -r 2
'''



from argparse import ArgumentParser
from sys import exit
from datetime import datetime
import pandas as pd
import numpy as np
import json



# scale factor for temperature correction
def scaleF(Tbare, Tmod):
    Eg = 1.22
    k = 8.617333262e-5

    def kelvin(T):
        return 273.15 + T

    return np.power(kelvin(Tmod) / kelvin(Tbare), 2) * np.exp(
        Eg * (Tmod - Tbare) / 2 / k / kelvin(Tmod) / kelvin(Tbare)
    )

# calculate breakdown voltage and leakage current at operation
def calc(tempdata, xdata, ydata, Vdepl):
    Vbd = -999
    Ilc = 0

    I_voltage_point = Vdepl + 50

    # correct leakage current at threshold voltage to 20 celsius
    TscaleF = 1
    if len(tempdata) == 1:
        TscaleF = scaleF(tempdata[0], 20)

    # Finding leakage current at threshold voltage
    for idx, V in enumerate(xdata):
        if V < Vdepl:
            continue
        elif V <= I_voltage_point:

            if len(tempdata) == len(xdata):
                TscaleF = scaleF(tempdata[idx], 20)
            Ilc = TscaleF * ydata[idx]

            if ydata[idx] > ydata[idx - 1] * 1.2 and xdata[idx - 1] != 0:
                Vbd = V
                print("Breakdown at {:.1f} V for planar sensor".format(Vbd))
                break
    return Vbd, Ilc



if __name__ == "__main__":
    parser = ArgumentParser(
        description="covent probe station IV data file to the formate used by WebApp."
    )
    parser.add_argument("-sn", dest="tile_sn", type=str, required=True, help="sensor tile serial number")
    parser.add_argument("-f", dest="raw_data", type=str, required=True, help="raw data file of probe station IV")
    parser.add_argument("-run", dest="run_no", type=str, default="1", help="run number")
    parser.add_argument("-t", dest="run_date", type=str, help="test date")
    parser.add_argument("-v", dest="v_depl", type=float, default=60, help="depletion voltage")
    args = parser.parse_args()

    tile_sn = args.tile_sn
    raw_data = args.raw_data
    run_no = args.run_no
    run_date = args.run_date
    v_depl = args.v_depl

    if len(tile_sn) != 14 or not tile_sn.startswith("20UP"):
        print("Wrong format serial number is given!")
        exit(0)

    if run_date is None:
        run_date = datetime.now().strftime("%Y-%m-%dT%H:%MZ")

    df = pd.read_csv(raw_data, skiprows=6, sep='\t', names=['t/s', 'U/V', 'Iavg/uA', 'Istd/uA', 'T/C', 'RH/%', 'Tchiller/C'])
    print(df.head())

    voltage = [-1*i for i in df['U/V'].values.tolist()]
    current = [-1e-6*i for i in df['Iavg/uA'].values.tolist()]
    sigme_current = [1e-6*i for i in df['Istd/uA'].values.tolist()]
    temperature = df['T/C'].values.tolist()
    humidity = df['RH/%'].values.tolist()

    Vbd, Ilc = calc(temperature, voltage, current, v_depl) 

    no_Vbd_observed = 'true' if Vbd == -999 else 'false'

    json_tile = {
            'component': tile_sn,
            'testType': 'IV_MEASURE',
            'institution': "GOETTINGEN",
            'runNumber': run_no,
            'date': run_date,
            'passed': "true",
            'problems': 'false',
            'properties':{
                'TEMP': np.mean(temperature),
                'HUM': np.mean(humidity)
                },
            'results':{
                'IV_ARRAY': {
                    'voltage': voltage,
                    'current': current,
                    'sigma current': sigme_current
                    },
                'BREAKDOWN_VOLTAGE': Vbd,
                'LEAK_CURRENT': Ilc,
                'MAXIMUM_VOLTAGE': float(np.max(voltage)),
                'NO_BREAKDOWN_VOLTAGE_OBSERVED': no_Vbd_observed
                }
            }

    print(json_tile)
    with open('sensor_iv_%s.json'%tile_sn, 'w') as outfile:
        json.dump(json_tile, outfile)




    
