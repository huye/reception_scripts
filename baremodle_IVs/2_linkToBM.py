#!/usr/bin/python3

'''
example:
    python 2_linkToBM.py -sn 20UPGB42200007  -l 64db03e9130e660042444aea
'''

from argparse import ArgumentParser
from sys import exit
from datetime import datetime
import json

if __name__ == "__main__":
    parser = ArgumentParser(
        description="covent probe station IV data file to the formate readble by the GUI tool."
    )
    parser.add_argument("-sn", dest="bm_sn", type=str, required=True, help="bare module serial number")
    parser.add_argument("-l", dest="iv_ln", type=str, required=True, help="link to IV test")
    parser.add_argument("-run", dest="run_no", type=str, default="1", help="run number")
    parser.add_argument("-t", dest="run_date", type=str, help="test date")
    args = parser.parse_args()

    bm_sn = args.bm_sn
    iv_ln = args.iv_ln
    run_no = args.run_no
    run_date = args.run_date

    if len(bm_sn) != 14 or not bm_sn.startswith("20UP"):
        print("Wrong format serial number is given!")
        exit(0)

    if run_date is None:
        run_date = datetime.now().strftime("%Y-%m-%dT%H:%MZ")

    json_bm = {
            'component': bm_sn,
            'testType': 'BARE_MODULE_SENSOR_IV',
            'institution': "GOETTINGEN",
            'runNumber': run_no,
            'date': run_date,
            'passed': "true",
            'problems': 'false',
            'results':{
                "LINK_TO_SENSOR_IV_TEST": iv_ln
                }
            }

    with open('bm_iv_ln_%s.json'%bm_sn, 'w') as outfile:
        json.dump(json_bm, outfile)


