
To upload bare module IV, one needs to upload IV to sensor tile and then add a link to bare module 

step.1 run 1_convertRawToJson.py to create a json, then upload 'single test' of sensor tile IV in webApp (https://itk-pdb-webapps-pixels.web.cern.ch/).

step.1 (alternative) run 1_alt_convertRawToTxt.py to create a txt file, then upload with the Sensor GUI (https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts/-/blob/pixel_preproduction_GUI/pixels/sensors_preproduction/GUI/UploadTestResult.py)

setp.2 notedown the id of uploaded test, prepare a json with 2_linkToBM.py, then upload 'single test' of baremodule IV in webApp.
